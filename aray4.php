<?php

$arrNilai = array("Asep"=>80,"Rukman"=>90,"Ujang"=>65,"Karsim"=>75);
echo "Menampilkan isi Array asosiatif dengan foreach : <br>";
foreach ($arrNilai as $nama => $nilai) {
    echo "Nilai $nama = $nilai<br>";
}

reset($arrNilai);
echo "<br>Menampilkan isi array asosiayif dengan WHILE dan LIST : <br>";
while (list($nama, $nilai) = each($arrNilai)) {
    echo "Nilai $nama = $nilai <br>";
}
?>